# Hello-Pester

Possibly overengineered examples of how to use BDD-Style Poweshell Unit Testing with the [Pester Framework](https://github.com/pester/Pester).

You can find a lot of resources either:
* In the Github wiki: 
  * https://github.com/Pester/Pester/wiki
* Or on Jakubs blog:
  * http://jakubjares.com/

**PS: Please note that the code in the gif does not reflect the masters' HEAD:**
![](./img/fizzbuzz.gif)
