
Function Test-FizzBuzz {
    [CmdletBinding()]
    PARAM(
        [Switch] $Fizz,
        [Switch] $Buzz,
        [int] $Number
    )

    $Result = & {
        If ($Fizz) { $Number % 3 -eq 0 }
        If ($Buzz) { $Number % 5 -eq 0 }
    }

    $Result -notcontains $False
}


Function Invoke-Fizbuz {
    PARAM(
        [int]$Number
    )

    $Strings = & {
        If ((Test-FizzBuzz -Fizz $Number)) { 'Fizz' }
        If ((Test-FizzBuzz -Buzz $Number)) { 'Buzz' }
    }

    If (-not $Strings) { $Strings = @("$Number") }
    
    Write-Output ($Strings -join '')
}


# 1..20 | ForEach-Object { Invoke-Fizbuz -Number $_ }