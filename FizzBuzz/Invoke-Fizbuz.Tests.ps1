﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$here\$sut"



Describe "Test-FizzBuzz" {
    Context "Fizz" {
        It "Returns True if Divisable by 3" {
            Test-FizzBuzz -Fizz -Number 3 | should Be $True
        }

        It "Returns False if NOT divisable by 3" {
            Test-FizzBuzz -Fizz -Number 4 | Should Be $False
        }
    }

    Context "Buzz" {
        It "Returns True if divisable by 5" {
            Test-FizzBuzz -Buzz -Number 5 | Should Be $True
        }

        It "Returns False if NOT divisable by 5" {
            Test-FizzBuzz -Buzz -Number 6 | Should Be $False
        }
        
    }

    Context "Fizz & Buzz" {
        It "Returns True if divisable by both 5 and 3" {
            Test-FizzBuzz -Fizz -Buzz -Number 15 | Should Be $True
        }

        It "Returns False if NOT divisable by either 5 or 3" {
            Test-FizzBuzz -Fizz -Buzz -Number 12 | Should Be $False
            Test-FizzBuzz -Fizz -Buzz -Number 5 | Should Be $False
        }
    }
}


Describe "Invoke-Fizbuz" {
    Context "FizzBuzz" {
        It "Returns a number if not divisable by either 3 or 5" {
            1..100 | Where { $_ % 3 -ne 0 -and $_ % 5 -ne 0 } | ForEach-Object {
                Invoke-Fizbuz -Number $_ | Should Be "$_"
            }
        }

        It "Returns 'fizz' if number divisible by 3 (but not 5)" {
            1..100 | Where { $_ % 3 -eq 0 -and $_ % 5 -ne 0 } | ForEach-Object {
                Invoke-Fizbuz -Number $_ | Should Be "Fizz"
            }
        }

        It "Returns 'buzz' if number divisible by 5 (but not 3)"{
            1..100 | Where { $_ % 5 -eq 0 -and $_ % 3 -ne 0  } | ForEach-Object {
                Invoke-Fizbuz -Number $_ | Should Be "Buzz"
            }
        }

        It "Returns 'fizzbuzz' if number divisible by 3 and 5"{
            1..100 | Where { $_ % 5 -eq 0 -and $_ % 3 -eq 0 } | ForEach-Object {
                Invoke-Fizbuz -Number 15 | should be "FizzBuzz"
            }
        }
    }
}
